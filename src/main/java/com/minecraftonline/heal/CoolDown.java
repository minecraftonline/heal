package com.minecraftonline.heal;

import java.util.HashMap;

public class CoolDown {
	
	/*
	For this HashMap, the keys are player names with either "_heal" or _"feed" at the end.
	The values are arrays with two items. The first item is the last time (Unix time) the
	command was used successfully. The second item is whether the player used the command
	on themselves or someone else.
	*/
	static HashMap<String, Object[]> coolDownMap = new HashMap<String, Object[]>();
	
	// This functions returns true if the player's cooldown does not exist or has expired.
	static boolean updateCoolDown(String playerName, String healOrFeed, boolean onThemselves, int coolDownTime) {
		long unixTimeNow = System.currentTimeMillis() / 1000L;
		if (coolDownMap.containsKey(playerName + "_" + healOrFeed)) {
			long duration = unixTimeNow - (long) coolDownMap.get(playerName + "_" + healOrFeed)[0];
			// If the difference in time between the present and when they last
			// used the command is less than or equal to the cooldown time, the
			// player won't be able to use the command.
			if (duration <= coolDownTime) {
				return false;
			}
		}
		coolDownMap.put(playerName + "_" + healOrFeed, new Object[] {unixTimeNow, onThemselves});
		return true;
	}
}
