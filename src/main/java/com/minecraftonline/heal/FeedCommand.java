package com.minecraftonline.heal;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

public class FeedCommand implements CommandExecutor {

	private static boolean playerFeedable(Player user) {
		// If they have a hunger effect or if they are not full, they can be fed.
		// Checking for this effect also removes it from the player.
		return ClearPotionEffects.clearSpecificEffect(user, "effect.hunger") || user.getFoodData().foodLevel().get() < 20;
	}

	private static void feedPlayer(Player user) {
		// Replenishes the food bar. If the player is below the custom saturation level,
		// they get set to that saturation level.
		user.offer(Keys.FOOD_LEVEL, 20);
		if (user.getFoodData().saturation().get() < CustomVariables.saturationLevel) {
			user.offer(Keys.SATURATION, CustomVariables.saturationLevel);
		}
	}

	@Override
	public CommandResult execute(CommandSource source, CommandContext args) throws CommandException {
		Optional<Player> player = args.getOne("player");
		// Only continues if the feed command originates from the console or from a player.
		if (source instanceof Player || source instanceof ConsoleSource) {
			boolean commandAvailable = true;
			boolean feedThemselves = true;
			// If the [player] argument for "/feed [player]" has been given, and if the
			// player being fed is not the player running the command, then /feed is being 
			// used to feed another player.
			if (player.isPresent() && source.getName() != player.get().getName()) {
				feedThemselves = false;
				// A player may enter a zone where they can no longer be fed. Only someone with
				// the "heal.override" permission would be able to feed them.
				if (!player.get().hasPermission("heal.feedable") && !source.hasPermission("heal.override")) {
					source.sendMessage(Text.of(TextColors.DARK_RED, "You cannot feed this player at the moment!"));
					return CommandResult.success();
				}
			} else {
				if (!source.hasPermission("heal.feedable") && !source.hasPermission("heal.override")) {
					source.sendMessage(Text.of(TextColors.DARK_RED, "You cannot feed yourself at the moment!"));
					return CommandResult.success();
				}
			}

			if (source.hasPermission("heal.override")) {
				// Players with this permission skip the cooldown.
			} else {
				// This function returns false if the player is still on their cooldown.
				commandAvailable = CoolDown.updateCoolDown(source.getName(), "feed", feedThemselves, CustomVariables.coolDownTimeFeed);
			}

			if (commandAvailable) {
				if (!feedThemselves) {
					if (HealCommand.isPlayerDead(player.get())) {
						source.sendMessage(Text.of(TextColors.DARK_RED, "You cannot feed a dead player!"));
						// No cooldown if the command was not effective
						CoolDown.coolDownMap.remove(source.getName() + "_feed");
					} else if (playerFeedable(player.get())) {
						feedPlayer(player.get());
						if (source instanceof ConsoleSource) {
							player.get().sendMessage(Text.of(TextColors.DARK_GREEN, "You have been fed!"));
						} else {
							player.get().sendMessage(Text.of(TextColors.DARK_GREEN, "You have been fed by " + source.getName(), "!"));
						}
						source.sendMessage(Text.of(TextColors.DARK_GREEN, "They have been fed."));
					} else {
						source.sendMessage(Text.of(TextColors.DARK_GREEN, "They are already full!"));
						CoolDown.coolDownMap.remove(source.getName() + "_feed");
					}
				} else {
					if (source instanceof ConsoleSource) {
						source.sendMessage(Text.of(TextColors.DARK_RED, "You can't feed the console!"));
					} else {
						Player src = (Player) source;
						if (HealCommand.isPlayerDead(src)) {
							source.sendMessage(Text.of(TextColors.DARK_RED, "You cannot feed yourself if you are dead!"));
							CoolDown.coolDownMap.remove(source.getName() + "_feed");
						} else if (playerFeedable(src)) {
							feedPlayer(src);
							src.sendMessage(Text.of(TextColors.DARK_GREEN, "You have been fed!"));
						} else {
							src.sendMessage(Text.of(TextColors.DARK_GREEN, "You are already full!"));
							CoolDown.coolDownMap.remove(source.getName() + "_feed");
						}
					}
				}
			} else {
				source.sendMessage(Text.of(TextColors.DARK_RED, "Your feed cooldown has not expired!"));
			}
			return CommandResult.success();
		}
		return CommandResult.empty();
	}
}

