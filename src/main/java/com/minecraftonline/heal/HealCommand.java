package com.minecraftonline.heal;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

public class HealCommand implements CommandExecutor {

	private static void punishPlayer(Player user) {
		double healthValue = user.health().get();
		// (If the user has more health than what is going to be deducted)
		if (healthValue > CustomVariables.healthDamagePenalty) {
			user.offer(Keys.HEALTH, healthValue - CustomVariables.healthDamagePenalty);
		} else {
			// /heal only kills the player if this variable is true.
			if (CustomVariables.healIsKillable) {
				user.offer(Keys.HEALTH, 0.0);
			} else {
				// If it is not true, this shall set the player's health
				// to half a heart instead.
				user.offer(Keys.HEALTH, 1.0);
			}
		}
	}

	private static boolean playerHealable(Player user) {
		// If they are poisoned or if they are not at full health, they can be healed.
		// Checking if they are poisoned also removes the poison effect.
		return ClearPotionEffects.clearSpecificEffect(user, "effect.poison") || user.getHealthData().health().get() < user.getHealthData().health().getMaxValue();
	}

	private static void healPlayer(Player user) {
		// Sets their health to max and extinguishes them if they are on fire.
		user.offer(Keys.HEALTH, user.getHealthData().health().getMaxValue());
		user.offer(Keys.FIRE_TICKS, 0);
	}

	static boolean isPlayerDead(Player user) {
		// The player is dead if they are at 0 hearts.
		return user.getHealthData().health().get() <= 0.0;
	}

	@Override
	public CommandResult execute(CommandSource source, CommandContext args) throws CommandException {
		Optional<Player> player = args.getOne("player");
		// Only continues if the heal command originates from the console or from a player.
		if (source instanceof Player || source instanceof ConsoleSource) {
			boolean commandAvailable = true;
			boolean healThemselves = true;
			// If the [player] argument for "/heal [player]" has been given, and if the
			// player being healed is not the player running the command, 
			// then /heal is being used to heal another player.
			if (player.isPresent() && source.getName() != player.get().getName()) {
				healThemselves = false;
				// A player may enter a zone where they can no longer be healed. 
				// Only someone with the "heal.override" permission would be able to heal them.
				if (!player.get().hasPermission("heal.healable") && !source.hasPermission("heal.override")) {
					source.sendMessage(Text.of(TextColors.DARK_RED, "You cannot heal this player at the moment!"));
					return CommandResult.success();
				}
			} else {
				if (!source.hasPermission("heal.healable") && !source.hasPermission("heal.override")) {
					source.sendMessage(Text.of(TextColors.DARK_RED, "You cannot heal yourself at the moment!"));
					return CommandResult.success();
				}
			}

			if (source.hasPermission("heal.override")) {
				// Players with this permission skip the cooldown.
			} else {
				// This function returns false if the player is still on their cooldown.
				commandAvailable = CoolDown.updateCoolDown(source.getName(), "heal", healThemselves, CustomVariables.coolDownTimeHeal);
				
			}

			if (commandAvailable) {
				if (!healThemselves) {
					if (isPlayerDead(player.get())) {
						source.sendMessage(Text.of(TextColors.DARK_RED, "You cannot heal a dead player!"));
						// No cooldown if the command was not effective
						CoolDown.coolDownMap.remove(source.getName() + "_heal");
					} else if (playerHealable(player.get())) {
						healPlayer(player.get());
						if (source instanceof ConsoleSource) {
							player.get().sendMessage(Text.of(TextColors.DARK_GREEN, "You have been healed!"));
						} else {
							player.get().sendMessage(Text.of(TextColors.DARK_GREEN, "You have been healed by " + source.getName(), "!"));
						}
						source.sendMessage(Text.of(TextColors.DARK_GREEN, "They have been healed."));
						//logs successful heal usage on another player
						Heal.getLogger().info(source.getName() + " healed " + player.get().getName());	
					} else {
						source.sendMessage(Text.of(TextColors.DARK_GREEN, "They are already at full health!"));
						CoolDown.coolDownMap.remove(source.getName() + "_heal");
					}
				} else {
					if (source instanceof ConsoleSource) {
						source.sendMessage(Text.of(TextColors.DARK_RED, "You can't heal the console!"));
					} else {
						Player src = (Player) source;
						if (isPlayerDead(src)) {
							source.sendMessage(Text.of(TextColors.DARK_RED, "You cannot heal yourself if you are dead!"));
							CoolDown.coolDownMap.remove(source.getName() + "_heal");
						} else if (playerHealable(src)) {
							healPlayer(src);
							src.sendMessage(Text.of(TextColors.DARK_GREEN, "You have been healed!"));
							//logs successful heal usage on self
							Heal.getLogger().info(src.getName() + " has healed themselves");
						} else {
							src.sendMessage(Text.of(TextColors.DARK_GREEN, "You are already at full health!"));
							CoolDown.coolDownMap.remove(source.getName() + "_heal");
						}
					}
				}
			} else {
				source.sendMessage(Text.of(TextColors.DARK_RED, "Your heal cooldown has not expired!"));
				// A health penalty won't occur if the player heals someone else and then tries
				// to heal themselves.
				// It also won't occur if they are trying to heal another player.
				if (healThemselves && (boolean) CoolDown.coolDownMap.get(source.getName() + "_heal")[1]) {
					punishPlayer((Player) source);
				}
			}
			return CommandResult.success();
		}
		return CommandResult.empty();
	}
}
